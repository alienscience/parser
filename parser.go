// Package parser provides parser combinators.
package parser

import (
	"bytes"
	"fmt"
	"math"
	"strings"
	"unicode"
	"unicode/utf8"
)

// State defines the input and output of a parser
type State struct {
	Todo    []byte
	Tokens  []Token
	Success bool
}

// P defines a parser interface.
type P interface {
	Parse(State) State
}

// Fn defines a parser function.
type Fn func(State) State

// TokenStream returns a stream of tokens emitted by a parser.
func (s State) TokenStream() *TokenStream {
	return &TokenStream{Tokens: s.Tokens}
}

// Parse parses the given input
func (f Fn) Parse(in State) State {
	return f(in)
}

// NewState wraps the given slice to create a new struct for parsing.
func NewState(buf []byte) State {
	return State{Todo: buf, Success: true}
}

// InStr wraps the given string to create a new struct for parsing.
func InStr(s string) State {
	return NewState([]byte(s))
}

// Emit adds a recognized token to the result
func Emit(parser P, token int) Fn {
	return func(in State) State {
		ret := parser.Parse(in)
		if ret.Success {
			s := in.Todo[:len(in.Todo)-len(ret.Todo)]
			ret.Tokens = append(ret.Tokens, Token{token, s})
		}
		return ret
	}
}

// Seq runs the given parsers in a sequence.
// Seq is successful if all the parsers are successful.
func Seq(parsers ...P) Fn {
	return func(in State) State {
		ret := in
		for _, parser := range parsers {
			ret = parser.Parse(ret)
			if !ret.Success {
				break
			}
		}
		return ret
	}
}

// OneOf tries each of the given parsers and is successful if any one
// of the parsers is successful.
func OneOf(parsers ...P) Fn {
	return func(in State) State {
		for _, parser := range parsers {
			ret := parser.Parse(in)
			if ret.Success {
				return ret
			}
		}
		in.Success = false
		return in
	}
}

// Repeat repeats the given parser while it is successful.
func Repeat(min, max int, parser P) Fn {
	return func(in State) State {
		ret := in
		for i := 0; i < max; i++ {
			ret = parser.Parse(ret)
			if !ret.Success {
				if i >= min {
					ret.Success = true
				}
				break
			}
		}
		return ret
	}
}

// RepeatWhile repeats the first parse only while the second parse is successful
// RepeatWhile uses backtracking. Min and Max parameters control the number of
// matches for the first parser.
// Returns the longest match of (first + second)
func RepeatWhile(min, max int, first P, second P) Fn {
	return func(in State) State {
		i := 0
		ret := in
		backtrack := make([]State, 1, 4)
		backtrack[0] = in
		// Run the first parser greedily
		for ; i < max; i++ {
			ret = first.Parse(ret)
			if !ret.Success {
				break
			}
			backtrack = append(backtrack, ret)
		}
		// Backtrack from end of first parser until the second parser
		// is successful
		for ; i >= min; i-- {
			ret = second.Parse(backtrack[i])
			if ret.Success {
				return ret
			}
		}
		ret.Success = false
		return ret
	}
}

// ZeroOrMoreLazy repeats the first parser and second parser
// until the second parser is not successful. This function uses
// backtracking and care must be taken when combined with the Action
// combinator.
func ZeroOrMoreLazy(first P, second P) Fn {
	return RepeatWhile(0, math.MaxInt32, first, second)
}

// OneOrMoreLazy repeats the first parser and the second parser
// until the second parser is not successful. This function uses
// backtracking and care must be taken when combined with the Action
// combinator.
func OneOrMoreLazy(first P, second P) Fn {
	return RepeatWhile(1, math.MaxInt32, first, second)
}

// Optional optionally runs the given parser. The returned parser is always successful.
func Optional(parser P) Fn {
	return Repeat(0, 1, parser)
}

// ZeroOrMore runs the given parser function zero or more times. The returned parser
// is always successful.
func ZeroOrMore(parser P) Fn {
	return Repeat(0, math.MaxInt32, parser)
}

// OneOrMore runs the given parser one or more times.
func OneOrMore(parser P) Fn {
	return Repeat(1, math.MaxInt32, parser)
}

// IsIn will be successful if the parser input rune is in the given string.
func IsIn(s string) Fn {
	return func(in State) State {
		ret := in
		ret.Success = false
		if len(ret.Todo) == 0 {
			return ret
		}
		rune, n := utf8.DecodeRune(ret.Todo)
		if strings.ContainsRune(s, rune) {
			ret.Success = true
			ret.Todo = ret.Todo[n:]
		}
		return ret
	}
}

// NotIn will be successful if the parser input rune is not in the given string.
func NotIn(s string) Fn {
	return func(in State) State {
		ret := in
		if len(ret.Todo) == 0 {
			ret.Success = false
			return ret
		}
		rune, n := utf8.DecodeRune(ret.Todo)
		if strings.ContainsRune(s, rune) {
			ret.Success = false
			return ret
		}
		ret.Todo = ret.Todo[n:]
		return ret
	}
}

// Range will be successful if the parser input rune is in the given range of
// runes (inclusive).
func Range(min, max rune) Fn {
	return func(in State) State {
		ret := in
		if len(ret.Todo) == 0 {
			ret.Success = false
			return ret
		}
		rune, n := utf8.DecodeRune(ret.Todo)
		if rune != utf8.RuneError && rune >= min && rune <= max {
			ret.Todo = ret.Todo[n:]
		} else {
			ret.Success = false
		}
		return ret
	}
}

// AnyRune matches any rune in the parser input.
func AnyRune() Fn {
	return func(in State) State {
		ret := in
		if len(ret.Todo) == 0 {
			ret.Success = false
			return ret
		}
		rune, n := utf8.DecodeRune(ret.Todo)
		if rune == utf8.RuneError {
			ret.Success = false
		} else {
			ret.Todo = ret.Todo[n:]
		}
		return ret
	}
}

// Lit matches the given string literal.
func Lit(s string) Fn {
	bytePrefix := []byte(s)
	return func(in State) State {
		ret := in
		if bytes.HasPrefix(in.Todo, bytePrefix) {
			ret.Todo = ret.Todo[len(bytePrefix):]
		} else {
			ret.Success = false
		}
		return ret
	}
}

// LitIgnoreCase matches the given string literal ignoring case.
func LitIgnoreCase(s string) Fn {
	lcCompare := strings.ToLower(s)
	return func(in State) State {
		ret := in
		end := len(lcCompare)
		if end > len(ret.Todo) {
			ret.Success = false
			return ret
		}
		inBytes := ret.Todo[:end]
		inStr := string(inBytes)
		lcIn := strings.ToLower(inStr)
		if lcIn == lcCompare {
			ret.Todo = ret.Todo[end:]
			return ret
		}
		ret.Success = false
		return ret
	}
}

// IsLetter is successful if the parser input is a letter rune.
func IsLetter() Fn {
	return func(in State) State {
		ret := in
		if len(ret.Todo) == 0 {
			ret.Success = false
			return ret
		}
		rune, n := utf8.DecodeRune(ret.Todo)
		if unicode.IsLetter(rune) {
			ret.Todo = ret.Todo[n:]
		} else {
			ret.Success = false
		}
		return ret
	}
}

// IsDigit is successful if the parser input is a digit rune.
func IsDigit() Fn {
	return func(in State) State {
		ret := in
		if len(ret.Todo) == 0 {
			ret.Success = false
			return ret
		}
		rune, n := utf8.DecodeRune(ret.Todo)
		if unicode.IsDigit(rune) {
			ret.Todo = ret.Todo[n:]
		} else {
			ret.Success = false
		}
		return ret
	}
}

// NonASCII is successful if the parser input rune is not an ascii character.
func NonASCII() Fn {
	return func(in State) State {
		ret := in
		if len(ret.Todo) == 0 {
			ret.Success = false
			return ret
		}
		rune, n := utf8.DecodeRune(ret.Todo)
		if rune != utf8.RuneError && rune > 0x7f {
			ret.Todo = ret.Todo[n:]
		} else {
			ret.Success = false
		}
		return ret
	}
}

// AlwaysFail is parser that always fails and will not parse any input.
func AlwaysFail(in State) State {
	ret := in
	ret.Success = false
	return ret
}

// Trace is a parser that always succeeds and prints the current parser state
// to stdout. This parser can be useful when debugging problems.
func Trace(msg string) Fn {
	return func(in State) State {
		end := len(in.Todo)
		if end > 20 {
			end = 20
		}
		fmt.Printf("%s > Todo=%s... Success=%v  Tokens=%#v\n",
			msg, in.Todo[:end], in.Success, in.Tokens)
		return in
	}
}

// ForwardRef holds a reference to another parser. ForwardRef exists as
// a workaround for recursive parsers see 'createParserForwardedToRef()' in:
// https://fsharpforfunandprofit.com/posts/understanding-parser-combinators-4/
type ForwardRef struct {
	ref P
}

// NewForwardRef creates a new ForwardRef. Initially the reference is to
// a dummy parser that always fails.
func NewForwardRef() *ForwardRef {
	return &ForwardRef{ref: Fn(AlwaysFail)}
}

// Parse parses the given input by forwarding to another parser.
func (f *ForwardRef) Parse(in State) State {
	return f.ref.Parse(in)
}

// Set sets the reference to forward to the given parser.
func (f *ForwardRef) Set(parser P) {
	f.ref = parser
}
