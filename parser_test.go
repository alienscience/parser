package parser

import (
	"testing"
)

func TestLit(t *testing.T) {
	p := Lit("abc")
	if !p.Parse(InStr("abc")).Success {
		t.Fail()
	}
	if p.Parse(InStr("ab")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestLitIgnoreCase(t *testing.T) {
	p := LitIgnoreCase("AbC")
	if !p.Parse(InStr("abc")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("ABC")).Success {
		t.Fail()
	}
	if p.Parse(InStr("ab")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestIsLetter(t *testing.T) {
	p := IsLetter()
	if !p.Parse(InStr("a")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("Ä")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("ß")).Success {
		t.Fail()
	}
	if p.Parse(InStr(".")).Success {
		t.Fail()
	}
	if p.Parse(InStr("-")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestIsDigit(t *testing.T) {
	p := IsDigit()
	if !p.Parse(InStr("1")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("٣")).Success {
		t.Fail()
	}
	if p.Parse(InStr(".")).Success {
		t.Fail()
	}
	if p.Parse(InStr("a")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestNonAscii(t *testing.T) {
	p := NonASCII()
	if !p.Parse(InStr("ö")).Success {
		t.Fail()
	}
	if p.Parse(InStr("3")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestAnyRune(t *testing.T) {
	p := AnyRune()
	if !p.Parse(InStr("abc")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("b")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestNotIn(t *testing.T) {
	p := NotIn("abc")
	if !p.Parse(InStr("def")).Success {
		t.Fail()
	}
	if p.Parse(InStr("a")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestIsIn(t *testing.T) {
	p := IsIn("abc")
	if !p.Parse(InStr("adef")).Success {
		t.Fail()
	}
	if p.Parse(InStr("d")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestRange(t *testing.T) {
	p := Range('c', 'z')
	if !p.Parse(InStr("cdef")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("z")).Success {
		t.Fail()
	}
	if p.Parse(InStr("b")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestOptional(t *testing.T) {
	p := Optional(Lit("a"))
	if !p.Parse(InStr("a")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("b")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestRepeatWhile(t *testing.T) {
	p := RepeatWhile(1, 3, Lit("a"), Lit("b"))
	if !p.Parse(InStr("ab")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("aab")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("aaab")).Success {
		t.Fail()
	}
	if p.Parse(InStr("aaaab")).Success {
		t.Fail()
	}
	if p.Parse(InStr("aaac")).Success {
		t.Fail()
	}
	if p.Parse(InStr("b")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestOneOrMoreLazy(t *testing.T) {
	p := OneOrMoreLazy(IsIn("ab"), Lit("b"))
	if !p.Parse(InStr("ab")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("aab")).Success {
		t.Fail()
	}
	if p.Parse(InStr("b")).Success {
		t.Fail()
	}
	if p.Parse(InStr("aaac")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestOneOrMore(t *testing.T) {
	p := OneOrMore(Lit("a"))
	if !p.Parse(InStr("aa")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("aab")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("a")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestZeroOrMore(t *testing.T) {
	p := ZeroOrMore(Lit("a"))
	if !p.Parse(InStr("aa")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("aab")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("b")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestSeq(t *testing.T) {
	p := Seq(Lit("aa"), Lit("bb"))
	if !p.Parse(InStr("aabb")).Success {
		t.Fail()
	}
	if p.Parse(InStr("aa")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestOneOf(t *testing.T) {
	p := OneOf(Lit("aa"), Lit("bb"))
	if !p.Parse(InStr("aa")).Success {
		t.Fail()
	}
	if !p.Parse(InStr("bb")).Success {
		t.Fail()
	}
	if p.Parse(InStr("c")).Success {
		t.Fail()
	}
	if p.Parse(InStr("")).Success {
		t.Fail()
	}
}

func TestEmit(t *testing.T) {
	p := Seq(
		Emit(Lit("aa"), 1),
		OneOf(
			Seq(
				Emit(Lit("bb"), 2),
				Lit("bb"),
			),
			Seq(
				Lit("bb"),
				Emit(Lit("cc"), 3),
			),
		),
	)
	res := p.Parse(InStr("aabbcc"))
	tokens := res.Tokens
	if !res.Success || len(tokens) != 2 ||
		tokens[0].ID != 1 || string(tokens[0].Capture) != "aa" ||
		tokens[1].ID != 3 || string(tokens[1].Capture) != "cc" {
		t.Fail()
	}
}

func TestTrace(t *testing.T) {
	p := Seq(Lit("aa"), Trace("trace"), Lit("bb"))
	res := p.Parse(InStr("aabb"))
	if !res.Success {
		t.Fail()
	}
}

func TestForwardRef(t *testing.T) {
	valueRef := NewForwardRef()
	list := Seq(
		Lit("["),
		valueRef,
		ZeroOrMore(Seq(Lit(","), valueRef)),
		Lit("]"),
	)
	value := OneOf(Lit("aa"), list)
	valueRef.Set(value)
	if !list.Parse(InStr("[aa,aa]")).Success {
		t.Fail()
	}
	if !list.Parse(InStr("[aa,aa,[aa,aa]]")).Success {
		t.Fail()
	}
	if !list.Parse(InStr("[[aa,aa,[aa,[aa,aa]]],aa]")).Success {
		t.Fail()
	}
}
