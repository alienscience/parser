# parser

A quick and dirty parser/combinator library in Go.

I wrote this library because I wanted a strict way of parsing SMTP commands coming into a mailserver.
In order to keep the parser code maintainable, I wanted something that would closely match the specification given in the SMTP RFC.

For instance:

```go
// helo = "HELO" SP Domain CRLF
func helo() parser.Fn {
	return parser.Seq(            // Sequence
		parser.Emit(
			parser.Lit("HELO"),   // Line starts with HELO
			heloCmd,              // Emit a token ID
		),
		sp(),                     // Function that parses a space
		parser.Emit(
			domain(),             // Function that parses a domain
			argToken,             // Emit a token ID
		),
	)
}
```

Running the parser results in a slice of tokens. A token, contains an ID and the bytes that were captured:

```go
type Token struct {
	ID      int
	Capture []byte
}
```

For instance, `{ID: heloCmd, Capture: []byte("HELO")}` or `{ID: argToken, Capture: []byte("")}`.

The slice of tokens can be can be used to build a datastructure. For complex data structures, recursive descent can be used.