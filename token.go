package parser

import "fmt"

// EOFToken is a token that signals that there are no more tokens available.
const EOFToken = -1

// Token identifies a slice of bytes
type Token struct {
	ID      int
	Capture []byte
}

// TokenStream wraps a slice of Token to provide helper methods.
type TokenStream struct {
	Tokens []Token
	i      int
}

// Next returns the next token. If no tokens are available, Next returns EOFToken.
func (s *TokenStream) Next() Token {
	if s.i >= len(s.Tokens) {
		return Token{ID: EOFToken}
	}
	ret := s.Tokens[s.i]
	s.i++
	return ret
}

// PutBack puts the last token back in the stream.
func (s *TokenStream) PutBack() {
	if s.i > 0 {
		s.i--
	}
}

// Match returns true and consumes a token if the next token matches the given ID.
func (s *TokenStream) Match(id int) bool {
	if s.i >= len(s.Tokens) {
		return id == EOFToken
	}
	ret := s.Tokens[s.i].ID == id
	if ret {
		s.i++
	}
	return ret
}

// Dump pretty prints a token stream for debugging purposes
func (s *TokenStream) Dump() {
	for _, tok := range s.Tokens {
		fmt.Printf("%02d %s\n", tok.ID, tok.Capture)
	}
}
